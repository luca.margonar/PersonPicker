
import controller.PersistenceController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import view.MainView;

import java.io.IOException;


public class UI extends Application {

    public static void main(String[] args) {
        launch(UI.class, args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        stage.setMaximized(true);

        MainView mainView = MainView.getInstance();

        mainView.setTalks(PersistenceController.readFromFile());
        BorderPane border = new BorderPane();

        HBox hbox = mainView.addHBox();
        border.setTop(hbox);
        border.setLeft(mainView.addFlowPaneLeft());

        border.setRight(mainView.addFlowPaneRight());

        border.setCenter(mainView.addAnchorPane(mainView.addGridPane()));

        Scene scene = new Scene(border);
        stage.setScene(scene);
        stage.setTitle("Person Picker");
        stage.show();
    }


}
