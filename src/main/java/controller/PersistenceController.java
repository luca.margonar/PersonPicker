package controller;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import model.Talk;
import view.MainView;


import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Margonar on 20.05.2017.
 */
public class PersistenceController {
    private static final String FILANAME = "data.txt";
    private static final Type REVIEW_TYPE = new TypeToken<List<Talk>>() {
    }.getType();

    public static void persistTalkList(ArrayList<Talk> obj) {
        Gson gson = new Gson();
        writeOnFile(gson.toJson(obj));
    }

    private static void writeOnFile(String json) {
        try {
            PrintWriter writer = new PrintWriter(FILANAME, "UTF-8");
            writer.println(json);
            writer.close();
        } catch (IOException e) {
            System.err.println("Ops! Something goes wrong...");
        }
    }

    public static ArrayList<Talk> readFromFile() throws IOException {
        Gson gson = new Gson();
        if (fileEmpty()) {
            return new ArrayList<Talk>();
        } else {
            JsonReader reader = new JsonReader(new FileReader(FILANAME));
            ArrayList<Talk> talks = gson.fromJson(reader, REVIEW_TYPE);
            MainView.getInstance().setSelected(talks.get(0));
            return talks;
        }


    }

    public static boolean fileEmpty() throws IOException {
        Reader reader = new FileReader(FILANAME);
        int readSize = reader.read();
        if (readSize == -1)
            return true;
        else
            return false;
    }
}
