package controller;

import model.Person;
import model.Talk;
import view.MainView;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Margonar on 20.05.2017.
 */
public class TalkController {

    private static final MainView mainView = MainView.getInstance();

    public static void addTalk(String name, LocalDate date){
        Talk talk = new Talk(name,new ArrayList<Person>(),date);
        mainView.getTalks().add(talk);
        mainView.getTalksList().add(talk);

        PersistenceController.persistTalkList(mainView.getTalks());
    }
}
