package controller;

import model.Person;
import view.MainView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Margonar on 20.05.2017.
 */
public class PersonController {

    private static final MainView mainView = MainView.getInstance();

    public static void personChangeState(Person person) {
        person.setPresent(!person.isPresent());
        PersistenceController.persistTalkList(mainView.getTalks());
    }

    public static boolean addPersonToTalk(String name, String surname) {
        if (mainView.getSelected() == null) {
            System.out.println("no talk selected");
            return false;
        } else {
            Person p = new Person(name, surname, true);
            ArrayList<Person> people = (ArrayList) mainView.getSelected().getPerson();
            people.add(p);
            mainView.getNames().add(p);
            PersistenceController.persistTalkList(mainView.getTalks());
            return true;
        }
    }

    public static Person randomPerson() {
        if (mainView.getSelected() == null || mainView.getSelected().getPerson().isEmpty()) {
            return null;
        }
        List<Person> partecipant = new ArrayList<>();
        for (Person person : mainView.getSelected().getPerson()) {
            if (person.isPresent()) {
                partecipant.add(person);
            }
        }
        if (partecipant.isEmpty()) {
            return null;
        } else {
            return partecipant.get(ThreadLocalRandom.current().nextInt(0, partecipant.size()));
        }

    }
}
