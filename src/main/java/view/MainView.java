package view;

import controller.PersonController;
import controller.TalkController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Callback;
import model.Person;
import model.Talk;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Created by Margonar on 20.05.2017.
 */
public class MainView {

    private ArrayList<Talk> talks = new ArrayList<>();
    private Talk selected = null;
    private Text chosenPerson;
    private ObservableList<Person> names;
    private ObservableList<Talk> talksList;
    private ListView<Person> listView;

    private static MainView instance = new MainView();

    private MainView() {
    }

    public static MainView getInstance() {
        return instance;
    }

    public HBox addHBox() {

        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);   // Gap between nodes
        hbox.setStyle("-fx-background-color: #336699;");

        Text title = new Text("Choose a person . . . ");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        hbox.getChildren().addAll(title);

        return hbox;
    }


    public GridPane addGridPane() {

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));


        Text title = new Text("Speaker: ");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(title, 1, 0);


        chosenPerson = new Text("");
        chosenPerson.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(chosenPerson, 2, 0);


        Text chartSubtitle = new Text("Some speaker action . . .");
        grid.add(chartSubtitle, 1, 1, 2, 1);


        return grid;
    }

    public FlowPane addFlowPaneRight() {

        FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 5, 5, 5));
        flow.setVgap(4);
        flow.setHgap(4);
        flow.setPrefWrapLength(200);
        flow.setStyle("-fx-background-color: DAE6F3;");

        names = FXCollections.observableArrayList();

        listView = new ListView<>(names);
        listView.setItems(null);

        listView.setPrefWidth(200);


        listView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        PersonController.personChangeState(listView.getSelectionModel().getSelectedItem());
                        listView.setItems(null);
                        listView.setItems(names);
                    }
                }
            }
        });


        Text participantTitle = new Text("Participant");
        participantTitle.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        flow.getChildren().add(participantTitle);
        flow.getChildren().add(new Label("Double tap on the\nparticipant name to exclude him"));
        flow.getChildren().add(listView);
        TextField name = new TextField();
        name.setPrefWidth(200);
        name.setPromptText("* Add name");
        TextField surname = new TextField();
        surname.setPrefWidth(200);
        surname.setPromptText("* Add surname");
        Button addPerson = new Button("Add Participant");
        Label insertPersonMessage = new Label("");
        insertPersonMessage.setTextFill(Color.DARKRED);
        addPerson.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (!name.getText().isEmpty() && !surname.getText().isEmpty()) {
                    if (PersonController.addPersonToTalk(name.getText(), surname.getText())) {
                        name.setText("");
                        surname.setText("");
                        insertPersonMessage.setText("");
                    } else {
                        insertPersonMessage.setText("Select a talk");
                    }
                } else {
                    insertPersonMessage.setText("All field are required");
                }
            }
        });

        flow.getChildren().add(insertPersonMessage);
        flow.getChildren().add(name);
        flow.getChildren().add(surname);
        flow.getChildren().add(addPerson);
        return flow;
    }


    public FlowPane addFlowPaneLeft() {

        FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 5, 5, 5));
        flow.setVgap(4);
        flow.setHgap(4);
        flow.setPrefWrapLength(200);
        flow.setStyle("-fx-background-color: DAE6F3;");

        talksList = FXCollections.observableArrayList(talks);
        ListView<Talk> listViewTalk = new ListView<Talk>(talksList);
        listViewTalk.setPrefWidth(200);
        listViewTalk.setCellFactory(new Callback<ListView<Talk>, ListCell<Talk>>() {
            @Override
            public ListCell<Talk> call(ListView<Talk> myObjectListView) {
                ListCell<Talk> cell = new ListCell<Talk>() {
                    @Override
                    protected void updateItem(Talk myObject, boolean b) {
                        super.updateItem(myObject, b);
                        if (myObject != null) {
                            setText("[" + myObject.getDate().format(DateTimeFormatter.ofPattern("dd.LL.yy")) + "] - " + myObject.getName());
                        }
                    }
                };

                return cell;
            }
        });

        listViewTalk.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    names.removeAll(names);
                    listView.setItems(null);
                    selected = listViewTalk.getSelectionModel().getSelectedItem();
                    if (!selected.getPerson().isEmpty()) {
                        names.setAll(selected.getPerson());
                        listView.setItems(names);
                        listView.setCellFactory(new Callback<ListView<Person>, ListCell<Person>>() {
                            @Override
                            public ListCell<Person> call(ListView<Person> myObjectListView) {
                                ListCell<Person> cell = new ListCell<Person>() {
                                    @Override
                                    protected void updateItem(Person myObject, boolean b) {
                                        super.updateItem(myObject, b);
                                        if (myObject != null) {
                                            if (!myObject.isPresent()) {
                                                setText(myObject.getName() + " " + myObject.getSurname());
                                                setTextFill(Color.LIGHTGRAY);
                                                setFont(Font.font(Font.getDefault().getName(), FontPosture.ITALIC, Font.getDefault().getSize()));
                                            } else {
                                                setText(myObject.getName() + " " + myObject.getSurname());
                                                setTextFill(Color.BLACK);
                                                setFont(Font.font(Font.getDefault().getName()));
                                            }

                                        }
                                    }
                                };

                                return cell;
                            }
                        });
                    }
                }
            }
        });
        Text talkTitle = new Text("Talks List");
        talkTitle.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        flow.getChildren().add(talkTitle);
        flow.getChildren().add(new Label("Tap the task to show participant"));
        flow.getChildren().add(listViewTalk);
        TextField name = new TextField();
        name.setPrefWidth(200);
        name.setPromptText("* Add talk title");

        DatePicker datePicker = new DatePicker();
        datePicker.setValue(LocalDate.now());
        datePicker.setPrefWidth(200);

        Button addTalks = new Button("Add Talk");
        Label message = new Label("");
        message.setTextFill(Color.DARKRED);
        addTalks.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (!name.getText().isEmpty()) {
                    TalkController.addTalk(name.getText(), datePicker.getValue());
                    name.setText("");
                    datePicker.setValue(LocalDate.now());
                    message.setText("");
                } else
                    message.setText("Talk name is required");
            }
        });
        flow.getChildren().add(message);
        flow.getChildren().add(name);
        flow.getChildren().add(datePicker);
        flow.getChildren().add(addTalks);
        return flow;
    }


    public AnchorPane addAnchorPane(GridPane grid) {

        AnchorPane anchorpane = new AnchorPane();

        Button generate = new Button("Choose Person");
        generate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Person p = PersonController.randomPerson();
                if (p != null) {
                    chosenPerson.setText(p.getName() + " " + p.getSurname());
                } else {
                    chosenPerson.setText("No participant on this talk");
                }
            }
        });


        HBox hb = new HBox();
        hb.setPadding(new Insets(0, 10, 10, 10));
        hb.setSpacing(10);
        hb.getChildren().addAll(generate);

        anchorpane.getChildren().addAll(grid, hb);
        // Anchor buttons to bottom right, anchor grid to top
        AnchorPane.setBottomAnchor(hb, 8.0);
        AnchorPane.setRightAnchor(hb, 5.0);
        AnchorPane.setTopAnchor(grid, 10.0);


        return anchorpane;
    }

    public ObservableList<Talk> getTalksList() {
        return talksList;
    }

    public void setTalksList(ObservableList<Talk> talksList) {
        this.talksList = talksList;
    }

    public Talk getSelected() {
        return selected;
    }

    public void setSelected(Talk selected) {
        this.selected = selected;
    }

    public ArrayList<Talk> getTalks() {
        return talks;
    }

    public void setTalks(ArrayList<Talk> talks) {
        this.talks = talks;
    }

    public Text getChosenPerson() {
        return chosenPerson;
    }

    public void setChosenPerson(Text chosenPerson) {
        this.chosenPerson = chosenPerson;
    }

    public ObservableList<Person> getNames() {
        return names;
    }

    public void setNames(ObservableList<Person> names) {
        this.names = names;
    }

    public ListView<Person> getListView() {
        return listView;
    }

    public void setListView(ListView<Person> listView) {
        this.listView = listView;
    }
}
