package model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Margonar on 19.05.2017.
 */
public class Talk {
    private String name;
    private List<Person> person;
    private LocalDate date;

    public Talk(String name, List<Person> person, LocalDate date) {
        this.name = name;
        this.person = person;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPerson() {
        return person;
    }

    public void setPerson(List<Person> person) {
        this.person = person;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

}
