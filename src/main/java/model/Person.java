package model;

/**
 * Created by Margonar on 19.05.2017.
 */
public class Person {
    private String name;
    private String surname;
    private boolean isPresent;

    public Person(String name, String surname, boolean isPresent) {
        this.name = name;
        this.surname = surname;
        this.isPresent = isPresent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }
}
